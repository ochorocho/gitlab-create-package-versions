<?php

namespace Ochorocho;

use Dotenv\Dotenv;
use Dotenv\Exception\InvalidPathException;
use Gitlab\Api\RepositoryFiles;
use Gitlab\Client;
use Gitlab\Exception\RuntimeException;
use Gitlab\ResultPager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class GitlabComposerPackagesCommand extends Command
{

    /**
     * API Connection
     *
     * @var Client $client
     */
    protected $client;

    /**
     * API Files
     *
     * @var RepositoryFiles $files
     */
    protected $files;

    /**
     * Env config
     *
     * @var array<Array|String> $config
     */
    protected $config;

    /**
     * OutputInterface
     *
     * @var OutputInterface $output
     */
    protected $output;

    /**
     * InputInterface
     *
     * @var InputInterface $input
     */
    protected $input;

    protected static $defaultName = 'gitlab:create-composer-packages';

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->input = $input;

        $this->loadConfig();
        $this->connectToGitlab();
        $this->files = new RepositoryFiles($this->client);
        $projects = $this->getComposerProjects();

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Create versions for the listed projects above [y|n] ? ', false, '/^(y|j)/i');

        if (!$helper->ask($input, $output, $question)) {
            return Command::SUCCESS;
        }

        foreach ($projects as $project) {
            $this->createVersion($project);
        }

        return Command::SUCCESS;
    }

    /**
     * Connect to Gitlab API
     */
    private function connectToGitlab(): void
    {
        $this->client = new Client();
        $this->client->setUrl($this->config['GITLAB_URL']);
        $this->client->authenticate($this->config['GITLAB_TOKEN'], Client::AUTH_HTTP_TOKEN);
    }

    /**
     * Get all projects containing composer.json in master
     *
     * @throws \Http\Client\Exception
     * @return array<Array>
     */
    private function getComposerProjects()
    {
        $parameters = [];
        if (isset($this->config['GITLAB_NAMESPACE'])) {
            $parameters = [
                [
                    'search' => $this->config['GITLAB_NAMESPACE'],
                ],
            ];
        }

        $pager = new ResultPager($this->client);
        $projects = $pager->fetchAllLazy(
            $this->client->projects(),
            'all',
            $parameters
        );

        $composerProjects = [];
        $this->output->writeln('<info>Get all composer based projects</info>');

        foreach ($projects as $project) {
            $this->output->write(' - ' . $project['id'] . ' : ' . $project['name']);
            if ($this->excludeFilter($project)) {
                $this->output->writeln(' - skipped');
                continue;
            }
            try {
                $this->files->getFile($project['id'], 'composer.json', 'master');
                $composerProjects[] = $project;

                $this->output->writeln(' - processing');
                $this->output->writeln('   ' . $project['web_url']);
            } catch (RuntimeException $exception) {
                // Catch Exception
            }
        }
        $this->output->writeln('');

        return $this->filterEmptyTags($composerProjects);
    }

    /**
     * Respect exclude regex
     *
     * @param array<Array|String> $project
     * @return bool
     */
    private function excludeFilter(array $project)
    {
        $regex = array_key_exists('INCLUDE_ONLY_REGEX', $this->config) ? $this->config['INCLUDE_ONLY_REGEX'] : '/.*/';

        if (@preg_match($regex, $project['path_with_namespace'], $matches) === false) {
            $this->output->writeln('<error>Invalid Regular Expression: ' . $this->config['INCLUDE_ONLY_REGEX'] . '</error>');
            exit();
        }

        if (!empty($project['path_with_namespace']) && $project['archived'] !== true && !empty($matches)) {
            return false;
        }

        return true;
    }

    private function loadConfig(): void
    {
        try {
            $config = Dotenv::createImmutable(getcwd());
            $this->config = $config->load();
        } catch (InvalidPathException $exception) {
            $this->output->writeln('<error>Could not find .env file in current folder. Please create config .env, see https://gitlab.com/ochorocho/gitlab-create-package-versions#example-env-file</error>');
            exit();
        }
    }

    /**
     * Get rid of all projects without any tags and print info about project and its tags
     *
     * @param array<Array> $projects
     * @return array<Array>
     */
    private function filterEmptyTags(array $projects)
    {
        $this->output->writeln('');
        $projectsWithTags = [];

        foreach ($projects as $project) {
            $tags = $this->getTags($project);

            if (!empty($tags)) {
                $this->output->writeln('<info>' . $project['path_with_namespace'] . '</info>');
                $this->output->writeln(implode(', ', array_column($tags, 'name', 'id')));
                $project['tags'] = $tags;
                $projectsWithTags[] = $project;
            }
        }

        return $projectsWithTags;
    }

    /**
     * Get all tags of a project
     *
     * @param array<int|string> $project
     * @return mixed
     */
    private function getTags(array $project)
    {
        return $this->client->tags()->all($project['id']);
    }

    /**
     * @param array<int|string|array> $project
     * @throws \Http\Client\Exception
     */
    private function createVersion(array $project): void
    {
        $this->ensurePackagesEnabled($project);
        $client = $this->client->getHttpClient();

        foreach (array_column($project['tags'], 'name', 'id') as $tag) {
            try {
                $this->files->getFile($project['id'], 'composer.json', $tag);
            } catch (RuntimeException $exception) {
                $this->output->writeln($project['path_with_namespace'] . ':' . $tag . ' : Could not create version, composer.json missing');
                return;
            }

            try {
                $request = $client->post('api/v4/projects/' . $project['id'] . '/packages/composer?tag=' . $tag);
            } catch (RuntimeException $e) {
                $this->output->writeln($e->getMessage());

                return;
            }

            if ($request->getStatusCode() === 201) {
                $this->output->writeln('Created Package ' . $project['path_with_namespace'] . ':' . $tag);
            } else {
                $this->output->writeln($project['path_with_namespace'] . ' ' . $request->getStatusCode());
            }
        }
    }

    /**
     * Make sure Package feature is enabled
     *
     * @param array<int|string> $project
     */
    private function ensurePackagesEnabled(array $project): void
    {
        if ($project['packages_enabled'] !== true) {
            $this->client->projects()->update($project['id'], ['packages_enabled' => true]);
        }
    }
}
